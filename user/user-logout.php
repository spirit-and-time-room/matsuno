<?php
  session_start();

  $_SESSION = array(); // セッション変数クリア
  session_destroy(); // セッション破壊

  header('Location: /blog-application/index.html');
?>
