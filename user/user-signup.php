<?php
  $dsn = 'mysql:dbname=blog_application; host:localhost; charset=utf8'; // データベース名，ホスト名，文字コード
  $user = 'owner'; // ユーザー名
  $password = 'me1UStRKiXVWcXYe'; // パスワード

  $error_message = ''; // エラーメッセージ初期化

  if (isset($_POST['signup'])) { // 登録ボタンが押された場合
    if (empty($_POST['username'])) { // ユーザー名の有無の確認
      $error_message = 'ユーザー名無し';
    } else if (empty($_POST['password'])) { // パスワードの有無の確認
      $error_message = 'パスワード無し';
    }

    if (!empty($_POST['username']) && !empty($_POST['password'])) {

      $hashed_password = password_hash($_POST['password'], PASSWORD_DEFAULT);

      try {
        $dbh = new PDO($dsn, $user, $password); // データベース接続
        $stmt = $dbh->prepare('INSERT INTO users(username, password) VALUES(?, ?)');
        $stmt->bindValue(1, $_POST['username'], PDO::PARAM_STR);
        $stmt->bindValue(2, $hashed_password, PDO::PARAM_STR);
        $stmt->execute();

        header('Location: /blog-application/user/user-login.php');
        exit();

      } catch (PDOException $e) {
        $error_message = 'データベース接続エラー';
        // echo $e->getMessage();

      } finally {
        $dbh = null;

      }
    }
  }
?>

<!DOCTYPE html>
<html lang="ja">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <link rel="stylesheet" href="/blog-application/common/css/reset.css">
  <link rel="stylesheet" href="/blog-application/common/css/common.css">
  <title>ユーザー登録 | Blog application</title>
</head>
<body>
  <header class="header">
    <h1>ユーザー登録</h1>
  </header>

  <p class="txt-error"><?php echo $error_message ?></p>
  <form action="" method="post">
    <fieldset>
      <legend>ユーザー情報入力</legend>
      <p class="txt-input">ユーザー名</p>
      <input type="text" name="username" required>
      <p class="txt-input">パスワード</p>
      <input type="password" name="password" required>
    </fieldset>
    <button type="submit" name="signup">登録</button>
  </form>

  <footer class="footer">
    <nav>
      <ul>
        <li><a href="/blog-application/index.html">TOPへ戻る</a></li>
        <li><a href="/blog-application/user/user-login.php">ログイン</a></li>
      </ul>
    </nav>
  </footer>
</body>
</html>
