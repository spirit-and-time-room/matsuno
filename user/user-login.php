<?php
  session_start();

  $dsn = 'mysql:dbname=blog_application; host:localhost; charset=utf8'; // データベース名，ホスト名，文字コード
  $user = 'owner'; // ユーザー名
  $password = 'me1UStRKiXVWcXYe'; // パスワード

  $error_message = ''; // エラーメッセージ初期化

  if (isset($_POST['login'])) { // ログインボタンが押された場合
    if (empty($_POST['username'])) { // ユーザー名の有無の確認
      $error_message = 'ユーザー名無し';
    } else if (empty($_POST['password'])) { // パスワードの有無の確認
      $error_message = 'パスワード無し';
    }

    if (!empty($_POST['username']) && !empty($_POST['password'])) {

      try {
        $dbh = new PDO($dsn, $user, $password); // データベース接続
        $stmt = $dbh->prepare('SELECT * FROM users ORDER BY password DESC');
        $stmt->execute();

        while($result = $stmt->fetch(PDO::FETCH_ASSOC)) {
          if ($_POST['username'] === $result['username']) { // username正誤確認
            if (password_verify($_POST['password'], $result['password'])) { // password正誤確認
              session_regenerate_id(true); // 新しくセッションIDを生成
              $_SESSION['username'] = $result['username'];

              header('Location: /blog-application/post/post-list.php');
              exit();

            }
          }
        }

        $error_message = 'ユーザー名またはパスワードに誤り';

      } catch (PDOException $e) {
        $error_message = 'データベース接続エラー';
        // echo $e->getMessage();

      } finally {
        $dbh = null;

      }
    }
  }
?>

<!DOCTYPE html>
<html lang="ja">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <link rel="stylesheet" href="/blog-application/common/css/reset.css">
  <link rel="stylesheet" href="/blog-application/common/css/common.css">
  <title>ログイン | Blog application</title>
</head>
<body>
  <header class="header">
    <h1>ログイン</h1>
  </header>

  <p class="txt-error"><?php echo $error_message ?></p>
  <form action="" method="post">
    <fieldset>
      <legend>ユーザー情報入力</legend>
      <p class="txt-input">ユーザー名</p>
      <input type="text" name="username" required>
      <p class="txt-input">パスワード</p>
      <input type="password" name="password" required>
    </fieldset>
    <button type="submit" name="login">ログイン</button>
  </form>

  <footer class="footer">
    <nav>
      <ul>
        <li><a href="/blog-application/index.html">TOPへ戻る</a></li>
        <li><a href="/blog-application/user/user-signup.php">ユーザー登録</a></li>
      </ul>
    </nav>
  </footer>
</body>
</html>
