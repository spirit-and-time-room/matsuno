<?php
  session_start();

  if (!isset($_SESSION['username'])) {
    header('Location: /blog-application/index.html');
    exit();
  }

  $dsn = 'mysql:dbname=blog_application; host:localhost; charset=utf8'; // データベース名，ホスト名，文字コード
  $user = 'owner'; // ユーザー名
  $password = 'me1UStRKiXVWcXYe'; // パスワード

  $error_message = ''; // エラーメッセージ初期化

  $delete_id = filter_input(INPUT_GET, 'id');

  try {
    $dbh = new PDO($dsn, $user, $password); // データベース接続
    $stmt = $dbh->prepare('DELETE FROM posts WHERE id = :id');
    $stmt->bindValue(':id', $delete_id, PDO::PARAM_INT);
    $stmt->execute();

    header('Location: /blog-application/post/post-list.php');
    exit();

    } catch (PDOException $e) {
      $error_message = 'データベース接続エラー';
      // echo $e->getMessage();

    } finally {
      $dbh = null;

    }
?>
