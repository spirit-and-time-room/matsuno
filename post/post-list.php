<?php
  session_start();

  if (!isset($_SESSION['username'])) {
    header('Location: /blog-application/index.html');
    exit();
  }
?>

<!DOCTYPE html>
<html lang="ja">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <link rel="stylesheet" href="/blog-application/common/css/reset.css">
  <link rel="stylesheet" href="/blog-application/common/css/common.css">
  <title>一覧 | Blog application</title>
</head>
<body>
  <header class="header">
    <h1>一覧</h1>
  </header>

  <p class="txt-error"><?php echo $error_message ?></p>
  <p>username: <?php echo $_SESSION['username']; ?></p>
  <table>
  <tr>
    <th>画像</th>
    <th>コメント</th>
    <th>操作</th>
  </tr>
    <?php
      $dsn = 'mysql:dbname=blog_application; host:localhost; charset=utf8'; // データベース名，ホスト名，文字コード
      $user = 'owner'; // ユーザー名
      $password = 'me1UStRKiXVWcXYe'; // パスワード

      $error_message = ''; // エラーメッセージ初期化

      try {
        $dbh = new PDO($dsn, $user, $password); // データベース接続
        $stmt = $dbh->prepare('SELECT * FROM posts ORDER BY username DESC');
        $stmt->execute();

        while($result = $stmt->fetch(PDO::FETCH_ASSOC)) {
          if ($_SESSION['username'] === $result['username']) { // ログインしているユーザーの画像のみ表示
            $id = $result['id'];
            echo '<tr><td><img src="' . $result['fullpath'] . '"></td>';
            echo '<td>' . $result['comment'] . '</td>';
            echo '<td><a href="/blog-application/post/post-delete.php?id=' . $id . '">削除</a></td><tr>';
          }
        }

      } catch (PDOException $e) {
        $error_message = 'データベース接続エラー';
        // echo $e->getMessage();

      } finally {
        $dbh = null;

      }
    ?>
  </table>

  <footer class="footer">
    <nav>
      <ul>
        <li><a href="/blog-application/index.html">TOPへ戻る</a></li>
        <li><a href="/blog-application/post/post-upload.php">アップロード</a></li>
        <li><a href="/blog-application/post/post-list.php">一覧</a></li>
        <li><a href="/blog-application/user/user-logout.php">ログアウト</a></li>
      </ul>
    </nav>
  </footer>
</body>

</html>
