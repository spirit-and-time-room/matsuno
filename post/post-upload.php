<?php
  session_start();

  if (!isset($_SESSION['username'])) {
    header('Location: /blog-application/index.html');
    exit();
  }

  $dsn = 'mysql:dbname=blog_application; host:localhost; charset=utf8'; // データベース名，ホスト名，文字コード
  $user = 'owner'; // ユーザー名
  $password = 'me1UStRKiXVWcXYe'; // パスワード

  $error_message = ''; // エラーメッセージ初期化

  if (isset($_POST['upload'])) { // 投稿ボタンが押された場合
    if (empty($_FILES['file']['name'])) { // ファイルの有無の確認
      $error_message = '画像ファイル無し';
    } else if (empty($_POST['comment'])) { // コメントの有無の確認
      $error_message = 'コメント無し';
    }

    if (!empty($_FILES['file']['name']) && !empty($_POST['comment'])) {
      $image_origin_name = $_FILES['file']['name'];
      $image_temporary_name = $_FILES['file']['tmp_name'];

      try {
        $dbh = new PDO($dsn, $user, $password); // データベース接続
        $stmt = $dbh->prepare('INSERT INTO posts(fullpath, comment) VALUES(?, ?)');
        $stmt->bindValue(1, '/blog-application/post/storage/image/' . $image_origin_name, PDO::PARAM_STR);
        $stmt->bindValue(2, $_POST['comment'], PDO::PARAM_STR);
        $stmt->execute();

        if (!move_uploaded_file($image_temporary_name, 'storage/image/' . $image_origin_name)) {
          $error_message = '画像アップロード失敗';
        }

        header('Location: /blog-application/post/post-list.php');
        exit();

      } catch (PDOException $e) {
        $error_message = 'データベース接続エラー';
        // echo $e->getMessage();

      } finally {
        $dbh = null;

      }
    }
  }
?>

<!DOCTYPE html>
<html lang="ja">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <link rel="stylesheet" href="/blog-application/common/css/reset.css">
  <link rel="stylesheet" href="/blog-application/common/css/common.css">
  <title>アップロード | Blog application</title>
</head>
<body>
  <header class="header">
    <h1>アップロード</h1>
  </header>

  <p class="txt-error"><?php echo $error_message ?></p>
  <form action="" method="post" enctype="multipart/form-data">
    <fieldset>
      <legend>アップロード</legend>
      <p class="txt-input">画像を添付</p>
      <input type="hidden" name="max_file_size" value="1000000">
      <input type="file" name="file" value="">
      <p class="txt-input">コメント</p>
      <textarea name="comment" rows="8" cols="80"></textarea>
    </fieldset>
    <button type="submit" name="upload">投稿</button>
  </form>

  <footer class="footer">
    <nav>
      <ul>
        <li><a href="/blog-application/index.html">TOPへ戻る</a></li>
        <li><a href="/blog-application/post/post-upload.php">アップロード</a></li>
        <li><a href="/blog-application/post/post-list.php">一覧</a></li>
        <li><a href="/blog-application/user/user-logout.php">ログアウト</a></li>
      </ul>
    </nav>
  </footer>
</body>
</html>
